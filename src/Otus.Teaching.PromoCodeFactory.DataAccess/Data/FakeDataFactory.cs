﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>
        {
            new Preference
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers => new List<Customer>
        {
            new Customer
            {
                Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                Email = "ivan_sergeev@mail.ru",
                FirstName = "Иван",
                LastName = "Петров",
                CustomerPreferences = new List<CustomerPreference>
                {
                    CustomerPreferences.FirstOrDefault(x => x.Id == Guid.Parse("d886fbf5-9dd3-4662-8f9c-37c495f21376"))
                    
                },
                PromoCodes = new List<PromoCode>
                {
                    PromoCodes.FirstOrDefault(x => x.Id == Guid.Parse("c9a14414-e04b-401a-a38f-9b97c2eebfba"))
                }
            },
            new Customer
            {
                Id = Guid.Parse("a708a080-bcd3-42e8-a907-327e6c4f3bce"),
                Email = "test_testov@gmail.com",
                FirstName = "Test",
                LastName = "Testov",
                CustomerPreferences = new List<CustomerPreference>
                {
                    CustomerPreferences.FirstOrDefault(x => x.Id == Guid.Parse("c5d5bff2-08f1-46e8-9f17-204aa01864bb"))
                }
            }
        };

        public static IEnumerable<CustomerPreference> CustomerPreferences => new List<CustomerPreference>
        {
            new CustomerPreference
            {
                Id = Guid.Parse("d886fbf5-9dd3-4662-8f9c-37c495f21376"),
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
            },
            new CustomerPreference
            {
                Id = Guid.Parse("c5d5bff2-08f1-46e8-9f17-204aa01864bb"),
                CustomerId = Guid.Parse("a708a080-bcd3-42e8-a907-327e6c4f3bce"),
                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd")
            }
        };

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>
        {
            new PromoCode
            {
                Id = Guid.Parse("c9a14414-e04b-401a-a38f-9b97c2eebfba"),
                Code = "Promo1qwe",
                ServiceInfo = "ServiceInfo1qwe",
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(1),
                PartnerName = "PartnerName1qwe",
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0")
            }
        };
    }
}