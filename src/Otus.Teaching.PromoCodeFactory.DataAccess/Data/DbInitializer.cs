﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public DbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            if (_dataContext.Database.EnsureCreated())
            {
                _dataContext.Preferences.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();

                _dataContext.Employees.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChangesAsync();
                
                _dataContext.Customers.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();
            }
            _dataContext.SaveChanges();
        }
    }
}