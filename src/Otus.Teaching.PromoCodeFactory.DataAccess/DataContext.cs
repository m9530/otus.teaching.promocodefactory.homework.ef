﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
            
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>();

            modelBuilder.Entity<Role>();
            
            modelBuilder.Entity<Preference>();
            
            modelBuilder.Entity<PromoCode>();

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new {cp.CustomerId, cp.PreferenceId});

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId);
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany()
                .HasForeignKey(cp => cp.PreferenceId);

            modelBuilder.Entity<Customer>()
                .HasMany(x => x.PromoCodes)
                .WithOne(x => x.Customer)
                .HasForeignKey(x => x.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);
            
            // Length restrictions
            modelBuilder.Entity<Employee>(e =>
            {
                e.Property(x => x.FirstName).HasMaxLength(256);
                e.Property(x => x.LastName).HasMaxLength(256);
                e.Property(x => x.LastName).HasMaxLength(512);
                e.Property(x => x.Email).HasMaxLength(256);
            });

            modelBuilder.Entity<Role>(r =>
            {
                r.Property(x => x.Name).HasMaxLength(256);
                r.Property(x => x.Description).HasMaxLength(512);
            });

            modelBuilder.Entity<Preference>(p =>
            {
                p.Property(x => x.Name).HasMaxLength(256);
            });

            modelBuilder.Entity<Customer>(c =>
            {
                c.Property(x => x.FirstName).HasMaxLength(256);
                c.Property(x => x.LastName).HasMaxLength(256);
                c.Property(x => x.LastName).HasMaxLength(512);
                c.Property(x => x.Email).HasMaxLength(256);
            });

            modelBuilder.Entity<PromoCode>(p =>
            {
                p.Property(x => x.Code).HasMaxLength(256);
                p.Property(x => x.PartnerName).HasMaxLength(256);
                p.Property(x => x.ServiceInfo).HasMaxLength(512);
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder){
            
            optionsBuilder.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}