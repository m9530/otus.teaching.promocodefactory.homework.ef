﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly EfRepository<Customer> _customerRepository;
        private readonly EfRepository<Preference> _preferenceRepository;
        private readonly EfRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly EfRepository<PromoCode> _promoCodeRepository;

        public CustomersController(EfRepository<Customer> customerRepository, EfRepository<Preference> preferenceRepository, EfRepository<CustomerPreference> customerPreferenceRepository, EfRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Get all customers short info.
        /// </summary>
        /// <returns>List of customers short info.</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerShortResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ICollection<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersShortResponseList = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).ToList();

            return customersShortResponseList;
        }
        
        /// <summary>
        /// Get customer by id.
        /// </summary>
        /// <returns>Customer info by id.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var customerPreferences = await _customerPreferenceRepository.GetAllAsync();
            var preferences = await _preferenceRepository.GetAllAsync();
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var customerResponse = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = promoCodes.Where(x => x.CustomerId == id)
                    .Select(x => new PromoCodeShortResponse
                    {
                        Id = x.Id,
                        Code = x.Code,
                        ServiceInfo = x.ServiceInfo,
                        BeginDate = x.BeginDate.ToString(CultureInfo.InvariantCulture),
                        EndDate = x.EndDate.ToString(CultureInfo.InvariantCulture),
                        PartnerName = x.PartnerName
                    }).ToList(),
                PreferenceResponses = customerPreferences.Where(x => x.CustomerId == id)
                    .Select(x => new PreferenceResponse
                    {
                        Id = x.PreferenceId, 
                        Name = preferences.Where(p => p.Id == x.PreferenceId).Select(p => p.Name).First()
                    }).ToList()
            };
            
            return customerResponse;
        }
        
        /// <summary>
        /// Create new customer.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            var preferenceIds = request.PreferenceIds;
            var allPreferences = await _preferenceRepository.GetAllAsync();

            var customerPreferences = (from preferenceId in preferenceIds 
                    let preference = allPreferences.FirstOrDefault(x => x.Id == preferenceId) 
                    where preference != null 
                    select new CustomerPreference
                    {
                        Id = Guid.NewGuid(), 
                        CustomerId = customer.Id, 
                        PreferenceId = preferenceId
                    })
                .ToList();

            customer.CustomerPreferences = customerPreferences;
            
            await _customerRepository.AddAsync(customer);
            
            return Ok(customer.Id);
        }
        
        /// <summary>
        /// Update a customer.
        /// </summary>
        /// <returns>Updated employee.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) throw new Exception(nameof(customer)+ " with id " + id + " is not found");
            
            if (request.FirstName != null) customer.FirstName = request.FirstName;
            if (request.LastName != null) customer.LastName = request.LastName;
            if (request.Email != null) customer.Email = request.Email;
            
            if (request.PreferenceIds.Any())
            {
                var customerPreferences = await _customerPreferenceRepository.GetAllAsync();
                foreach (var customerPreference in customerPreferences)
                {
                    if (customerPreference.CustomerId == id)
                    {
                        await _customerPreferenceRepository.DeleteAsync(customerPreference);
                    }
                }
                
                var updatedCustomerPreferences = new List<CustomerPreference>();
                foreach (var updatedCustomerPreference in request.PreferenceIds.Select(requestPreferenceId => 
                             new CustomerPreference
                             {
                                 Id = Guid.NewGuid(),
                                 CustomerId = customer.Id,
                                 PreferenceId = requestPreferenceId
                             }))
                {
                    await _customerPreferenceRepository.AddAsync(updatedCustomerPreference);
                    updatedCustomerPreferences.Add(updatedCustomerPreference);
                }
                
                customer.CustomerPreferences = updatedCustomerPreferences;
            }

            await _customerRepository.UpdateAsync(customer);

            return Ok("updated");
        }
        
        /// <summary>
        /// Delete a customer.
        /// </summary>
        /// <returns>No content.</returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            await _customerRepository.DeleteAsync(customer);
            
            var customerPreferences = await _customerPreferenceRepository.GetAllAsync();
            foreach (var customerPreference in customerPreferences)
            {
                if (customerPreference.CustomerId == id)
                {
                    await _customerPreferenceRepository.DeleteAsync(customerPreference);
                }
            }

            var promoCodes = await _promoCodeRepository.GetAllAsync();
            foreach (var promoCode in promoCodes)
            {
                if (promoCode.CustomerId == id)
                {
                    await _promoCodeRepository.DeleteAsync(promoCode);
                }
            }
            
            return NoContent();
        }
    }
}