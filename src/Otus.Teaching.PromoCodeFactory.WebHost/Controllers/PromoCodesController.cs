﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private readonly EfRepository<PromoCode> _promoCodeRepository;
        private readonly EfRepository<Preference> _preferenceRepository;
        private readonly EfRepository<Customer> _customerRepository;
        private readonly EfRepository<CustomerPreference> _customerPreferenceRepository;

        public PromoCodesController(EfRepository<PromoCode> promoCodeRepository, EfRepository<Preference> preferenceRepository, EfRepository<Customer> customerRepository, EfRepository<CustomerPreference> customerPreferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            return promoCodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString(CultureInfo.InvariantCulture),
                EndDate = x.EndDate.ToString(CultureInfo.InvariantCulture),
                PartnerName = x.PartnerName
            }).ToList();
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(x => x.Name == request.Preference);

            if (preference == null) throw new Exception(nameof(preference));

            var allCustomers = await _customerRepository.GetAllAsync();

            var allCustomerPreferences = await _customerPreferenceRepository.GetAllAsync();
            var customerPreferences = allCustomerPreferences
                .Where(customerPreference => customerPreference.PreferenceId == preference.Id).ToList();

            var customersWithPreferences = new List<Customer>();
            foreach (var customer in allCustomers)
            {
                customersWithPreferences.AddRange(from customerPreference in customerPreferences 
                    where customer.Id == customerPreference.CustomerId select customer);
            }

            var promoCodes = new List<PromoCode>();
            foreach (var customer in customersWithPreferences)
            {
                var promoCode = new PromoCode
                {
                    Id = Guid.NewGuid(),
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    PreferenceId = preference.Id,
                    CustomerId = customer.Id
                };
                promoCodes.Add(promoCode);
                
                await Task.WhenAll(_promoCodeRepository.AddAsync(promoCode));

                await Task.WhenAll(_customerRepository.UpdateAsync(customer));
            }

            return Ok();
        }
    }
}